use smallvec::*;
use typenum::*;

use crate::Board;
use crate::Intersection;

impl<S: Unsigned> Board<S> {
    /// Converts a line and column and size into a 1D index
    pub fn lc_to_index(line: usize, column: usize) -> usize {
        let size = Board::<S>::line_internal_size();
        line * size + column
    }
    /// Converts a 1D index to a line and column index
    pub fn i_to_line_col(intersection: Intersection) -> (usize, usize) {
        let size = Board::<S>::line_internal_size();
        (intersection / size, intersection % size)
    }

    /// Returns neighboring intersections, this works as a toric board so it will overflow for oob
    /// intersections.
    pub fn neighboring_intersections(intersection: Intersection) -> [Intersection; 4] {
        let (i, j) = Board::<S>::i_to_line_col(intersection);
        [
            Board::<S>::lc_to_index(i, j - 1),
            Board::<S>::lc_to_index(i, j + 1),
            Board::<S>::lc_to_index(i - 1, j),
            Board::<S>::lc_to_index(i + 1, j),
        ]
    }
    pub fn occupied_neighbors(&self, intersection: Intersection) -> SmallVec<[Intersection; 4]> {
        Board::<S>::neighboring_intersections(intersection)
            .iter()
            .filter(|i| !self.empty_intersections.contains(**i))
            .copied()
            .collect()
    }
    pub fn free_neighbors(&self, intersection: Intersection) -> SmallVec<[Intersection; 4]> {
        Board::<S>::neighboring_intersections(intersection)
            .iter()
            .filter(|i| self.empty_intersections.contains(**i))
            .copied()
            .collect()
    }
}
