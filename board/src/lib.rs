use smallvec::*;
use typenum::*;

mod bitboards;
mod neighbors;

use bitboards::Bitboard;

use neighbors::*;

//pub type OOBSize<S> = Sum<S, U2>;
//pub type OOBSquareSize<S> = Prod<OOBSize<S>, OOBSize<S>>;

/// A predefined type for 9x9 board
pub type Board9x9 = Board<U9>;

/// A player type
pub type Player = bool;
/// Black player constant
pub const BLACK: Player = false;
/// White player constant
pub const WHITE: Player = true;

pub type Intersection = usize;

/// The main structure representing a board generic over a square size
#[derive(Clone)]
pub struct Board<S: Unsigned> {
    // Bitboards for stone occupation
    /// stone occupation, indexed by player
    pub occupied_intersections: [Bitboard<S>; 2],
    /// empty intersections of the board
    pub empty_intersections: Bitboard<S>,
    /// sentinel intersections outside the board
    pub out_of_bound_intersections: Bitboard<S>,

    // Groups tracking
    /// A set of groups on the board
    pub strings: Vec<Group>,
    /// Index of the groupe of each intersection, if the intersection is empty then this
    /// information is meaningless
    // TODO update this to generic array when stabilized
    pub string_index: Vec<usize>,

    /// Player to move
    pub to_move: Player,
    /// Ko intersection if there is one
    pub ko: Option<Intersection>,
}

/// A string of stone
#[derive(Clone)]
pub struct Group {
    stones: SmallVec<[Intersection; 4]>,
    liberties: SmallVec<[Intersection; 8]>,
}

impl<S: Unsigned> Board<S> {
    /// Creates a new empty black to play board
    pub fn empty() -> Self {
        // Remember that the bitboards are of S+2 lines
        // We generate the empty intersections first so the junk of the bitboard is
        // set to out of bound
        let s = <S as Unsigned>::to_usize();
        let mut empty_intersections = Bitboard::empty();
        for line in 1..s + 1 {
            for column in 1..s + 1 {
                empty_intersections.set(line * (s + 2) + column);
            }
        }

        let mut out_of_bound_intersections = empty_intersections.clone();
        out_of_bound_intersections.flip_whole();
        Board {
            occupied_intersections: [Bitboard::empty(), Bitboard::empty()],
            empty_intersections,
            out_of_bound_intersections,

            strings: vec![],
            // TODO change this to fixed array size
            string_index: vec![0; Bitboard::<S>::total_size()],
            to_move: BLACK,
            ko: None,
        }
    }

    /// Tries to play a move and returns wether the move was legal and played or not
    // TODO return illegality reason
    pub fn play_move(&mut self, intersection: Intersection) -> bool {
        // The move is on an empty intersection
        if !self.empty_intersections.contains(intersection) {
            return false;
        }
        // The move is not forbidden by ko rule
        if self
            .ko
            .map_or(false, |ko_intersection| ko_intersection == intersection)
        {
            return false;
        }
        // Then tries to capture some stones
        // Remove liberties to adjacent strings
        let occupied_neighbors = self.occupied_neighbors(intersection);
        for intersection in occupied_neighbors {
            let string_index = self.string_index[intersection];
            self.strings[string_index].remove_liberty(intersection);
        }
        // The move is not self capturing
        // Play the move and change side to play
        true
    }

    /// Creates a new group with the given single stone
    fn create_group_from_stone(&self, stone: Intersection) -> Group {
        Group {
            stones: smallvec![stone],
            liberties: smallvec![], // TODO
        }
    }

    /// Returns the size of a line or column taking oob into account
    pub fn line_internal_size() -> usize {
        <S as Unsigned>::to_usize() + 2
    }
}

impl Group {
    //
    fn remove_liberty(&mut self, liberty: Intersection) {
        //
    }
}
