use std::ops::{BitAnd, BitAndAssign, BitOr, BitOrAssign};

use typenum::*;

// TODO bound overflow

/// A square Bitboard of N bits side
/// There are two sentinel lines and columns for oob neighbors
/// N has to be greater than 0
#[derive(Clone)]
pub struct Bitboard<N: Unsigned> {
    words: Vec<u64>,
    _phantom: std::marker::PhantomData<N>,
}

impl<N: Unsigned> Bitboard<N> {
    /// Creates an empty bitboard
    pub fn empty() -> Self {
        Self {
            words: vec![0; (Self::total_size() - 1) / 64 + 1],
            _phantom: std::marker::PhantomData,
        }
    }

    // Bitboard manipulation

    /// Sets the ith bit of the Bitboard
    #[inline]
    pub fn set(&mut self, index: usize) {
        self.words[index / 64] |= 1 << (index % 64);
    }
    /// Unsets the ith bit of the Bitboard
    #[inline]
    pub fn unset(&mut self, index: usize) {
        self.words[index / 64] &= !(1 << (index % 64));
    }
    /// Flip the whole bitboard (equivalent to a not to itself)
    pub fn flip_whole(&mut self) {
        for word in self.words.iter_mut() {
            *word = !*word;
        }
    }

    // Bitboard access

    /// Returns wether or not the given bit is set
    #[inline]
    pub fn contains(&self, index: usize) -> bool {
        self.words[index / 64] & (1 << (index % 64)) != 0
    }
    /// Returns wether the given Bitboard is empty
    pub fn is_empty(&self) -> bool {
        self.words.iter().all(|&w| w == 0)
    }
    /// An iterator over the bits of a Bitboard
    pub fn iter(&self) -> impl Iterator<Item = bool> {
        BitBoardIterator {
            bb: self.clone(),
            index: 0,
        }
    }
    /// Returns the total square size of the bitboard taking into account oob intersections
    pub fn total_size() -> usize {
        // Take into account oob intersections
        let size = <N as Unsigned>::to_usize() + 2;
        size * size
    }
}

struct BitBoardIterator<N: Unsigned> {
    bb: Bitboard<N>,
    index: usize,
}

impl<N: Unsigned> Iterator for BitBoardIterator<N> {
    type Item = bool;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index < Bitboard::<N>::total_size() {
            let result = self.bb.contains(self.index);
            self.index += 1;
            Some(result)
        } else {
            None
        }
    }
}

impl<N: Unsigned> std::fmt::Debug for Bitboard<N> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for (index, value) in self.iter().enumerate() {
            // Line break
            if index % (<N as Unsigned>::to_usize() + 2) == 0 && index > 0 {
                writeln!(f)?;
            }
            write!(f, "{}", value as u8)?;
        }
        writeln!(f)
    }
}

/// Intersection between two same size sets of bits
impl<N: Unsigned> BitAnd for Bitboard<N> {
    type Output = Self;
    fn bitand(mut self, rhs: Self) -> Self::Output {
        for (word, other_word) in self.words.iter_mut().zip(rhs.words.iter()) {
            *word &= other_word
        }
        self
    }
}

/// Intersection and assignation between two same size sets of bits
impl<N: Unsigned> BitAndAssign for Bitboard<N> {
    fn bitand_assign(&mut self, rhs: Self) {
        for (word, other_word) in self.words.iter_mut().zip(rhs.words.iter()) {
            *word &= other_word
        }
    }
}

/// Union between two same size sets of bits
impl<N: Unsigned> BitOr for Bitboard<N> {
    type Output = Self;
    fn bitor(mut self, rhs: Self) -> Self::Output {
        for (word, other_word) in self.words.iter_mut().zip(rhs.words.iter()) {
            *word |= other_word
        }
        self
    }
}

/// Union and assignation between two same size sets of bits
impl<N: Unsigned> BitOrAssign for Bitboard<N> {
    fn bitor_assign(&mut self, rhs: Self) {
        for (word, other_word) in self.words.iter_mut().zip(rhs.words.iter()) {
            *word |= other_word
        }
    }
}
